
// Playing Cards
// Matt Nelson

#include <iostream>
#include <conio.h>

using namespace std;

enum Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit
{
	HEARTS,
	DIAMONDS,
	CLUBS,
	SPADES
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	cout << "The ";
	switch (card.rank)
	{
		case 2: cout << "two"; break;
		case 3: cout << "three"; break;
		case 4: cout << "four"; break;
		case 5: cout << "five"; break;
		case 6: cout << "six"; break;
		case 7: cout << "seven"; break;
		case 8: cout << "eight"; break;
		case 9: cout << "nine"; break;
		case 10: cout << "ten"; break;
		case 11: cout << "jack"; break;
		case 12: cout << "queen"; break;
		case 13: cout << "king"; break;
		case 14: cout << "ace"; break;
	}

	cout << " of ";
	switch (card.suit)
	{
		case 1: cout << "hearts"; break;
		case 2: cout << "diamonds"; break;
		case 3: cout << "clubs"; break;
		case 4: cout << "spades"; break;
	}
}

Card HighCard(Card c1, Card c2)
{
	if (c1.rank >= c2.rank) return c1;
	return c2;
}

int main()
{

	//Tim's code


	//Matt's code
	Card card;
	card.rank = THREE;
	card.suit = DIAMONDS;
	PrintCard(card);

	//cout << card.rank << ", " << card.suit;

	_getch();
	return 0;
}
